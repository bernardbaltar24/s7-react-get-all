import React, {useState, useEffect} from 'react';
import { Container, Row, Col } from 'reactstrap';
import TasksForm from '../forms/TasksForm';
import TasksTableHead from '../tables/TasksTableHead';
import axios from 'axios';


const TasksPage = (props) => {

  console.log("TasksPage props", props.token)

  const[tasksData, setTasksData] = useState({
      token: props.token,
      tasks: []
    })

    const {token, tasks} = tasksData;

    console.log("TASKSSPAGE22 TOKEN", token)

    const getTasks = async () => {
      try{
        const config = {
        headers : {
          Authorization: `Bearer ${token}`
        }
      }

        const res = await axios.get("http://localhost:5000/tasks", config)
        console.log("TASKS PAGE res", res)

        return setTasksData({
          tasks: res.data
        })
      }catch(e){
        console.log("TASKS ERROR", e)
      }
    }
  
  useEffect(() => {
    getTasks()
  },[setTasksData])

  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>Tasks Page</h1>
        </Col>
      </Row>
      <Row className="d-flex">
        <Col md="4" className="border">
        	<TasksForm/>
        </Col>
        <Col md="8" className="">
        	<TasksTableHead tasksAttr={tasks} />
        </Col>
      </Row>
    </Container>
  );
}

export default TasksPage;