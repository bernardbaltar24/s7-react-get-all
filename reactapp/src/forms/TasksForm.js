import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const Example = (props) => {
  return (
    <Form>
      <FormGroup>
        <Label for="exampleEmail" className="mt-3">Description</Label>
        <Input type="" name="" id="" placeholder="Please Input Description" />
      </FormGroup>
      <FormGroup>
        <Label for="">Member Id</Label>
        <Input type="" name="" id="" placeholder="Please Input Member Id" />
      </FormGroup>

      <div className="text-center"><Button className="mb-3 primary">Create Task</Button></div>
    </Form>
  );
}

export default Example;