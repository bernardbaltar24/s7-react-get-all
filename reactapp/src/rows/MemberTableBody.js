import React, { Fragment } from 'react';
import { Table } from 'reactstrap';

const MembersTableRow = ({membersAttr, index}) => {
  console.log("MEMBER ROW ATTR", membersAttr)

  const {username, email, id, teamId, position}=membersAttr;
  return (
    <Fragment>
        <tr>
          <th scope="row">{index}</th>
          <td>{username}</td>
          <td>{email}</td>
          <td>{teamId ? teamId.name: "N/A"}</td>
          <td>{position}</td>
          <td>
            <button className="btn btn-primary ml-1"><i class="far fa-eye"></i></button>
            <button className="btn btn-warning ml-1"><i class="far fa-edit"></i></button>
            <button className="btn btn-danger ml-1"><i class="far fa-trash-alt"></i></button>
          </td>
        </tr>
    </Fragment>
  );
}

export default MembersTableRow;