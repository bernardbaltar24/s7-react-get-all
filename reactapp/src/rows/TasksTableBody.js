import React, { Fragment } from 'react';
import { Table } from 'reactstrap';

const TaskRow = ({tasksAttr, index}) => {
  console.log("TASKS ROW ATTR", tasksAttr)

  const {description, memberId}=tasksAttr;
  return (
    <Fragment>
        <tr>
          <th scope="row">{index}</th>
          <td>{description}</td>
          <td>{memberId ? memberId.firstName+" "+memberId.lastName: "N/A"}</td>
          <td>
            <button className="btn btn-primary ml-1"><i class="far fa-eye"></i></button>
            <button className="btn btn-warning ml-1"><i class="far fa-edit"></i></button>
            <button className="btn btn-danger ml-1"><i class="far fa-trash-alt"></i></button>
          </td>
        </tr>
    </Fragment>
  );
}

export default TaskRow;