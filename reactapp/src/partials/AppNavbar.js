import React, { useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

const AppNavbar = (props) => {
  console.log("appnavbar props", props.token)
  console.log("appnavbar props", props.username)
  console.log("appnavbar props getCurrentMember", props.getCurrentMember)
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);


let authLinks = "";

if(!props.token){
  authLinks = (
    <Fragment>
      <Link to="/login" className="btn btn-primary">Login</Link>
      <Link to="/register" className="btn btn-primary">Register</Link>
    </Fragment>
    )
} else {
  authLinks = (
    <Fragment>
      <Link to="/profile" className="btn btn-outline">Welcome, {props.username}!</Link>
      <Link to="/logout" className="btn btn-secondary">Logout</Link>
      <Link to="/profile" className="btn btn-outline">Profile</Link>
    </Fragment>
    )
}

let bodyLinks = "";

if(!props.token){
  bodyLinks = (
    <Fragment>
        <NavbarBrand className="font-weight-bold text-white" href="/">MERN Tracker</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
        <Nav className="mr-auto" navbar></Nav>
            {authLinks}
        </Collapse>
    </Fragment>
    )
} else {
  bodyLinks = (
    <Fragment>
        <NavbarBrand className="font-weight-bold text-white" href="/">MERN Tracker</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <Link to="/members" className="nav-link">Members</Link>
            </NavItem>
            <NavItem>
              <Link to="/teams" className="nav-link">Teams</Link>
            </NavItem>
            <NavItem>
              <Link to="/tasks" className="nav-link">Tasks</Link>
            </NavItem>
          </Nav>
            {authLinks}
        </Collapse>
    </Fragment>
    )
}

  return (
    <div className = "mb-5">
      <Navbar color="light" light expand="md" className="bg-dark">
        {bodyLinks}
      </Navbar>
    </div>
  );
}

export default AppNavbar;