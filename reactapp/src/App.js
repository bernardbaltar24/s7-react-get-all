import React, { useState } from 'react';
import {Redirect, BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar' //kahit walang extension kasi js din sya.
import MembersPage from './pages/MembersPage';
import TeamsPage from './pages/TeamsPage';
import TasksPage from './pages/TasksPage';
import NotFoundPage from './pages/NotFoundPage';
import LogInPage from './pages/LogInPage';
import LandingPage from './pages/LandingPage';
import ProfilePage from './pages/ProfilePage';
import RegisterPage from './pages/RegisterPage';


const App = () => {

	const [appData, setAppData] = useState({
		token: localStorage.token,
		username: localStorage.username
	})

	const{token, username} = appData;
	console.log(username)
	// console.log(username)

	const getCurrentMember = () =>{
		return {username, token}
	}

	const Load = (props, page) => {

		//if for landing page		

		if(page === "LandingPage"){return <LandingPage {...props} />}
		
		//LOGIN & REGISTER PAGE

		if(page === "LogInPage" && token){return <Redirect to="/profile" />}
		else if(page === "LogInPage") {return <LogInPage {...props} />}

		if(page === "RegisterPage" && token){return <Redirect to="/profile" />} 
		else if(page === "RegisterPage") {return <RegisterPage {...props} />}

		if(!token) return <Redirect to="/login" />


		if( page === "LogoutPage"){
			localStorage.clear()
			setAppData({
				token,
				username
			})
			return window.location="/login"
		}

		switch(page){
			case "MembersPage": return <MembersPage {...props} token={token} />
			case "ProfilePage": return <ProfilePage {...props} getCurrentMember={getCurrentMember()} />
			case "TeamsPage": return <TeamsPage {...props} token={token} />
			case "TasksPage": return <TasksPage {...props} token={token} />
			{/*case "LogInPage": return <LogInPage {...props} token={token} />
			case "RegisterPage": return <RegisterPage {...props} token={token} /> */}
			default: return <NotFoundPage/>
		}
	}
	

	return (
	<BrowserRouter>
		<AppNavbar token={token} username={username} getCurrentMember={getCurrentMember()} />
		<Switch>	
			<Route path="/members" render={ (props) => Load(props, "MembersPage") } />
			<Route path="/profile" render={ (props) => Load(props, "ProfilePage")} />
			<Route path="/teams" render={ (props) => Load(props, "TeamsPage")} />
			<Route path="/tasks" render={ (props) => Load(props, "TasksPage")} />
			<Route path="/login" render={ (props) => Load(props, "LogInPage")} />
			<Route path="/register" render={(props)=>Load(props, "RegisterPage")} />
			<Route path="/logout" render={(props)=>Load(props, "LogoutPage")} />
			<Route path="/" render={(props)=>Load(props, "LandingPage")} />
			<Route path="*" render={ (props) => Load(props, "NotFoundPage") } />
			
		</Switch>
	</BrowserRouter>
)}


export default App;